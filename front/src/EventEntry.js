import React from 'react';
import {withRouter} from 'react-router';
import {Form, Input, Button, Typography} from 'antd';

const {Title} = Typography;
const {TextArea} = Input;

class EventEntry extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            eventName: '',
            description: '',
            candidates: ''
            // ここにstateの初期値を書きます
        };
    }

    registerEvent = async(evt) => {
        evt.preventDefault();
        try {
            const res = await fetch('/api/events/', {
                method: "POST",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(this.state)
            });
            const data = await res.json();
            this
                .props
                .history
                .push(`event/${data.eventId}`);
        } catch (e) {
            console.log(e);
        }
    }
    onChangeEventName = (evt) => {
        const eventName = evt.target.value;
        this.setState({eventName: eventName})
        // 入力された値(evt.target.value)をsetStateします
    }

    onChangeDescription = (evt) => {
        const description = evt.target.value;
        this.setState({description: description})
    }

    onChangeCandidates = (evt) => {
        const candidates = evt.target.value;
        this.setState({candidates: candidates})
    }

    // <div>内に入力フォームを作ります
    render() {
        return (
            <div>
                <Typography>
                    <Title level={2}>イベント登録</Title>
                </Typography>
                <Form onSubmit={this.registerEvent}>
                    <Form.Item>
                        <Input
                            placeholder="送別会"
                            onChange={this.onChangeEventName}
                            value={this.state.eventName}/>
                        <Input
                            placeholder="送別会の日程調整をしましょう"
                            onChange={this.onChangeDescription}
                            value={this.state.description}/>
                        <TextArea
                            value={this.state.candidates}
                            onChange={this.onChangeCandidates}
                            placeholder='8/7(月) 20:00～&#13;&#10;8/8(火) 20:00～&#13;&#10;8/9(水) 21:00～'></TextArea>
                        <Button type="primary" htmlType="submit">送信</Button>
                    </Form.Item>
                </Form>
            </div>
        );
    }
}

export default withRouter(EventEntry);